#include <iostream>

using namespace std;

class prisera{
private:
    int m_zivot;

public:
    prisera(int zivot){
        m_zivot = zivot;

    }
    int getZivot(){
        if (m_zivot>0)
        return m_zivot;
        else {
            cout << "prisera je mrtva" << endl;
            return 0;
        }
    }

    void snizZivot(int kolikZivotu){
        m_zivot = m_zivot - kolikZivotu;

    }
};

class hrdina {
private:
    string m_jmeno;
    int m_utok;
    int m_zivot;


public:
    static int s_poc;

    hrdina (string jmeno, int utok, int zivot){
        m_jmeno = jmeno;
        m_utok = utok;
        m_zivot = zivot;
        hrdina::s_poc = hrdina::s_poc+1;
    }

    string getJmeno(){
        return m_jmeno;

    }

    int getUtok(){
        return m_utok;
    }

    ~hrdina(){
        hrdina::s_poc = hrdina::s_poc-1;
    }

    static int pocet(){
        return s_poc;

    }

    void zautoc(prisera* nepritel){
        nepritel->snizZivot(getUtok());

    }
};



int hrdina::s_poc = 0;


int main() {
    hrdina *hrdina1 = new hrdina("artus",5,100);
    hrdina *hrdina2 = new hrdina("bum",20,100);
    hrdina *hrdina3 = new hrdina("bac",10,100);
    hrdina *hrdina4 = new hrdina("bam",75,100);
    //cout<<hrdina::s_poc<<endl;
    //delete hrdina1;
    //cout<<hrdina::s_poc<<endl;
    //cout<<hrdina::pocet()<<endl;
    prisera* prisera1 = new prisera(100);
    hrdina1->zautoc(prisera1);
    hrdina2->zautoc(prisera1);

    cout<< prisera1->getZivot() <<endl;
    hrdina4->zautoc(prisera1);
    cout<< prisera1->getZivot() <<endl;

    return 0;
}