//
// Created by Komaba on 05/12/2018.
//

#ifndef PROJEKT_HRAC_H
#define PROJEKT_HRAC_H

#include <iostream>
#include <vector>
#include "Zbran.h"
#include "Armor.h"
#include "Lektvar.h"


class Hrac {
    std::string m_jmeno;
    std::string m_povolani;
    int m_sila;
    int m_obrana;
    int m_zdravi;
    int m_jidlo;
    std::vector<Zbran*> m_zbrane;
    std::vector<Lektvar*> m_lektvary;
    std::vector<Armor*> m_armory;
public:

    Hrac(std::string jmeno, std::string povolani, int sila, int obrana, int zdravi, int jidlo);
    int getUtok();
    int getObrana();
    int getZdravi();
    int getJidlo();
    void seberZbran(Zbran* zbran);
    void seberLektvar(Lektvar* lektvar);
    void seberArmor(Armor* armor);
    void vypijLektvar();
    void printInfo();

};


#endif //PROJEKT_HRAC_H
