//
// Created by Komaba on 05/12/2018.
//

#include "Hrac.h"

Hrac::Hrac(std::string jmeno, std::string povolani, int sila, int obrana, int zdravi, int jidlo) {

    m_jmeno = jmeno;
    m_povolani = povolani;
    m_sila = sila;
    m_obrana = obrana;
    m_zdravi = zdravi;
    m_jidlo = jidlo;
}

int Hrac::getUtok() {
    return  m_sila;
}


int Hrac::getZdravi() {
    return m_zdravi + m_obrana;
}

int Hrac::getJidlo() {
    return m_jidlo;
}

void Hrac::seberZbran(Zbran *zbran) {
    m_zbrane.push_back(zbran);
}

void Hrac::seberLektvar(Lektvar *lektvar) {
    m_lektvary.push_back(lektvar);
}

void Hrac::seberArmor(Armor *armor) {
    m_armory.push_back(armor);
}

void Hrac::vypijLektvar() {
    m_lektvary.pop_back();
}
void Hrac::printInfo() {
    std::cout<<"jmeno "<<m_jmeno<<std::endl;
    std::cout<<"povolani "<<m_povolani<<std::endl;
    std::cout<<"sila "<<m_sila<<std::endl;
    std::cout<<"zdravi "<<m_zdravi<<std::endl;
    std::cout<<"jidlo "<<m_jidlo<<std::endl;

}

