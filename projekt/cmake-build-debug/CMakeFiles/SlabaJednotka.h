//
// Created by xpetkov on 28.11.2018.
//

#ifndef PROJEKT_SLABAJEDNOTKA_H
#define PROJEKT_SLABAJEDNOTKA_H

#include <iostream>
#include "Jednotka.h"

class SlabaJednotka : public Jednotka {
    int m_hlad;


    int getHlad();
    SlabaJednotka(int hlad, int zdravi, int sila);

    void printInfo();
    void PostavBudovu(int jakou);
    void Pracuj(std::string kde);

};


#endif //PROJEKT_SLABAJEDNOTKA_H
