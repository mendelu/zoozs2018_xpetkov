//
// Created by xpetkov on 28.11.2018.
//

#ifndef PROJEKT_JEDNOTKA_H
#define PROJEKT_JEDNOTKA_H

#include <iostream>

class Jednotka {
protected:
    int m_zdravi;
    int m_sila;

public:
    int getZdravi();
    int getSila();
    Jednotka(int zdravi,int sila);

    virtual void printInfo() = 0;
    virtual void PostavBudovu(int jakou)=0;
    virtual void Pracuj(std::string kde)=0;

    //static Jednotka* getJednotka(int zdravi, int  sila);
};


#endif //PROJEKT_JEDNOTKA_H
