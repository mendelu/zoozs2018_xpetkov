//
// Created by xpetkov on 19.11.2018.
//

#ifndef INC_9_TYDNE_PRACOVNIPOZICE_H
#define INC_9_TYDNE_PRACOVNIPOZICE_H


class pracovniPozice {
public:
    virtual int getPlat(int vzdelani, int zkusenosti) = 0;
    virtual int getPocetDniDovolene(int kolikByloCerpano)= 0;

};


#endif //INC_9_TYDNE_PRACOVNIPOZICE_H
