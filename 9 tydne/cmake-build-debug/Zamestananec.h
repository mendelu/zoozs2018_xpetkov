//
// Created by xpetkov on 19.11.2018.
//

#ifndef INC_9_TYDNE_ZAMESTANANEC_H
#define INC_9_TYDNE_ZAMESTANANEC_H

#include  "pracovniPozice.h"
#include  "Technik.h"
#include  "Akademik.h"
#include <iostream>
enum class TypyPozic{
    Akademik,Technik
};


class Zamestananec  {
    int m_vzdelani;
    int m_kolikCerpal;
    int m_zkusenosti;
    pracovniPozice* m_pracovniPozice;
public:
    Zamestananec(int vzdelani, int zkusenosti, int kolikDnuDovolene,std::string pracovniPozice);

    void zmenPozici(std::string);
    int getPlat();
    int getPocetDniDovolene();
    int getVzdelani();
    int getZkusenosti();

};


#endif //INC_9_TYDNE_ZAMESTANANEC_H
