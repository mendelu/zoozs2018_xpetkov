//
// Created by xpetkov on 19.11.2018.
//

#include "Technik.h"

int Technik::getPocetDniDovolene(int kolikByloCerpano) {
    return s_maxDniDovolene - kolikByloCerpano;
}

int Technik::getPlat(int vzdelani, int zkusenosti) {
    return s_platovyZaklad + zkusenosti * s_bonus;
}
