#include <iostream>
#include <array>

using namespace std;

class Kontejner{
    int m_vaha;
    string m_obsah;
    string m_majitel;
public:
    Kontejner(int vaha,string obsah,string majitel){
        m_vaha = vaha;
        m_obsah = obsah;
        m_majitel = majitel;
    }

    int getVaha(){
        return m_vaha;
    }

    string getMajitel(){
        return m_majitel;
    }

    string getObsah(){
        return m_obsah;
    }

    void printInfo(){
        cout<<"majitel je: "<< m_majitel<<endl;
        cout<<"obsah je: "<< m_obsah<<endl;
        cout<<"vaha je: "<< m_vaha<<endl;

    }
};

class Patro{
    string m_oznaceni;
    array<Kontejner*,10> m_pozice;
public:
    Patro(string oznaceni){
        m_oznaceni = oznaceni;

        for (Kontejner* &k:m_pozice){
            k =nullptr;
        }

    }
    void pridejKontejner(int pozice, Kontejner*ukladanyKontejner){
        m_pozice[pozice] = ukladanyKontejner;
    }

    void vypisKontejnery(){
        int poc = 0;
        for (Kontejner* k:m_pozice){
            cout<<"Na pozici " <<poc << endl;

            if (k== nullptr, poc++){
                cout<< "nic tam neni"<<endl;


            }
            else
                k->printInfo();


        }
    }

};

int main() {
    Kontejner*kon = new Kontejner(5,"ponozky","ja");
    Patro* p1 = new Patro ("P1");
    p1->pridejKontejner(0,kon);
    p1->vypisKontejnery();


    return 0;
}