//
// Created by Komaba on 05/12/2018.
//

#ifndef PROJEKT_ARMOR_H
#define PROJEKT_ARMOR_H

#include <iostream>

class Armor {
    std::string m_jmeno;
    int m_bonusObrana;

public:
    Armor(std::string jmeno, int bonusObrana);
    std::string getJmeno();
    int getBonusObrana();

};


#endif //PROJEKT_ARMOR_H
