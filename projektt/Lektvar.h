//
// Created by Komaba on 05/12/2018.
//

#ifndef PROJEKT_LEKTVAR_H
#define PROJEKT_LEKTVAR_H


class Lektvar {
    int m_bonusUtok;
    int m_bonusObrana;
public:
    Lektvar(int bonusUtok, int bonusObrana);
    int getBonusUtok();
    int getBonusObrana();
};


#endif //PROJEKT_LEKTVAR_H
