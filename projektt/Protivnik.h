//
// Created by Komaba on 10/12/2018.
//

#ifndef PROJEKTT_PROTIVNIK_H
#define PROJEKTT_PROTIVNIK_H

#include <iostream>
#include "Postava.h"

class Protivnik: public Postava {
private:

public:
    Protivnik(std::string jmeno, int sila, int zdravi, int lvl);


   std::string getJmeno();
   int getSilaUtoku();
   int getZraneni(int kolik);
   int getZdravi();
   bool Zije();
   void printInfo();
    //Protivnik::getProtivnik(int levelHrace);
};



#endif //PROJEKTT_PROTIVNIK_H
