//
// Created by Komaba on 05/12/2018.
//

#include "Armor.h"

Armor::Armor(std::string jmeno, int bonusObrana) {
    m_jmeno = jmeno;
    m_bonusObrana = bonusObrana;
}

std::string Armor::getJmeno() {
    return m_jmeno;
}

int Armor::getBonusObrana() {
    return m_bonusObrana;
}
