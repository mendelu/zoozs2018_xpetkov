//
// Created by Komaba on 12/12/2018.
//

#ifndef PROJEKTT_POSTAVA_H
#define PROJEKTT_POSTAVA_H


#include <iostream>

class Postava {
protected:
        std::string m_jmeno;
        int m_sila;
        int m_zdravi;
        int m_lvl;

    public:
        Postava(std::string jmeno, int sila, int zdravi, int lvl);
        virtual std::string getJmeno();
        virtual int getSilaUtoku();
        virtual int getZraneni(int kolik);
        virtual int getZdravi();
        virtual bool Zije();
        virtual void printInfo();

    };




#endif //PROJEKTT_POSTAVA_H
