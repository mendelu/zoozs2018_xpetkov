//
// Created by Komaba on 10/12/2018.
//

#ifndef PROJEKTT_WARBUILDER_H
#define PROJEKTT_WARBUILDER_H

#include <HracBuilder.h>
class WarBuilder:public HracBuilder {
public:
        WarBuilder();
        void createHrac(std::string jmeno);
        void generujZbran();
        void generujArmor();
        void generujLektvar();

};




#endif //PROJEKTT_WARBUILDER_H
