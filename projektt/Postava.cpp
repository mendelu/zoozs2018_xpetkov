//
// Created by Komaba on 12/12/2018.
//

#include "Postava.h"

Postava::Postava(std::string jmeno, int sila, int zdravi, int lvl) {
    m_jmeno = jmeno;
    m_sila = sila * lvl;
    m_zdravi = zdravi * lvl;
    m_lvl = lvl;
}

bool Postava::Zije() {
    return m_zdravi > 0;
}

int Postava::getSilaUtoku() {
    return m_sila;
}

int Postava::getZraneni(int kolik) {
    return m_zdravi -= kolik;
}

int Postava::getZdravi() {
    return m_zdravi;
}

std::string Postava::getJmeno() {
    return m_jmeno;
}

void Postava::printInfo() {
    std::cout<<"jmeno:      "<<m_jmeno<<std::endl;
    std::cout<<"sila:       "<<m_sila<<std::endl;
    std::cout<<"zdravi:     "<<m_zdravi<<std::endl;
    std::cout<<"uroven:     "<<m_lvl<<std::endl;

}