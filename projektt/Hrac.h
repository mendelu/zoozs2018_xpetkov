//
// Created by Komaba on 05/12/2018.
//

#ifndef PROJEKT_HRAC_H
#define PROJEKT_HRAC_H

#include <iostream>
#include <vector>
#include "Zbran.h"
#include "Armor.h"
#include "Lektvar.h"
#include "Protivnik.h"
#include "Postava.h"


class Hrac: public Postava {
    // pokud bych chtel commnad tak tady pridam vektor interakci (typy utoku, atp.)
    // plus pridat abstrakni class Interakce a pak konkretni implementace v potomcich.

    std::string m_povolani;
    int m_obrana;
    int m_jidlo;
    std::vector<Zbran*> m_zbrane;
    std::vector<Lektvar*> m_lektvary;
    std::vector<Armor*> m_armory;
public:

    Hrac(std::string jmeno, std::string povolani, int sila, int zdravi, int obrana, int lvl);
    int getSilaUtoku();
    int getZraneni(int kolik);
    int getLvl();
    int getJidlo();
    int getZdravi() override;
    void seberZbran(Zbran* zbran);
    void seberLektvar(Lektvar* lektvar);
    void seberArmor(Armor* armor);
    void vypijLektvar();
    void printInfo();
    void Boj(Protivnik* skym);
    bool Zije();
    void setZdravi();
    void LvlUp();
};


#endif //PROJEKT_HRAC_H
