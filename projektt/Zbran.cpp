//
// Created by Komaba on 05/12/2018.
//

#include "Zbran.h"

Zbran::Zbran(std::string jmeno, int bonusUtok) {
    m_jmeno = jmeno;
    m_bonusUtok = bonusUtok;
}

std::string Zbran::getJmeno() {
    return m_jmeno;
}

int Zbran::getBonusUtok() {
    return m_bonusUtok;
}