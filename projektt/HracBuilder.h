//
// Created by Komaba on 08/12/2018.
//

#ifndef PROJEKTT_HRACBUILDER_H
#define PROJEKTT_HRACBUILDER_H


#include "Hrac.h"

class HracBuilder {
protected:
    Hrac* m_hrac;
public:
    HracBuilder();
    virtual void createHrac(std::string jmeno)=0;
    virtual void generujZbran() =0;
    virtual void generujArmor()=0;
    virtual void generujLektvar()=0;
    Hrac * getHrac();


};


#endif //PROJEKTT_HRACBUILDER_H
