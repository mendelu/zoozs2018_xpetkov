//
// Created by Komaba on 08/12/2018.
//

#ifndef PROJEKTT_HRACDIRECTORY_H
#define PROJEKTT_HRACDIRECTORY_H

#include <iostream>
#include "HracBuilder.h"
#include "Postava.h"

enum povolani{
    mag,war
};


class HracDirector {
    HracBuilder* m_builder;

public:
    HracDirector(HracBuilder* builder);
    void setBuilder(HracBuilder* builder);
    Hrac* createHrdina(std::string jmeno);
};


#endif //PROJEKTT_HRACDIRECTORY_H
