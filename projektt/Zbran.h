//
// Created by Komaba on 05/12/2018.
//

#ifndef PROJEKT_ZBRAN_H
#define PROJEKT_ZBRAN_H

#include <iostream>

class Zbran {
    std::string m_jmeno;
    int m_bonusUtok;
public:
    Zbran(std::string jmeno, int bonusUtok);
    std::string getJmeno();
    int getBonusUtok();
};




#endif //PROJEKT_ZBRAN_H
