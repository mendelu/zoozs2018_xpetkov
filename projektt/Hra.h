//
// Created by Mikulas Muron on 11/12/2018.
//

#ifndef PROJEKTT_HRA_H
#define PROJEKTT_HRA_H

#include "Postava.h"
#include "Hrac.h"
#include "Protivnik.h"
#include <vector>
#include "MagBuilder.h"
#include "WarBuilder.h"
#include "HracDirectory.h"
#include "ProtivnikBuilder.h"

class Hra {
    // nekde tady si drzet ukazatel na hrace a na nepratele, nejaky vector nejspis.
    Hrac* m_hrac;
 static   std::vector<Protivnik*> m_nepratele;

public:
    // tady budu mit kod z mainu z radku 22.
    //static std::string zvolTypHrace();
    static Hrac* vytvotHrace();
    static void hraj();
    static void uvod();
    static void printInfo(Hrac* nekdo);
    static void coChces(Hrac * hrac);
    static void bojovani(Hrac* hrac);
    static void konec();
    static Protivnik* enemy(Hrac* hrac);
};


#endif //PROJEKTT_HRA_H
