//
// Created by Komaba on 08/12/2018.
//

#include "MagBuilder.h"

MagBuilder::MagBuilder() {

}

void MagBuilder::createHrac(std::string jmeno ) {
    m_hrac = new Hrac(jmeno, "Mag", 20, 10,100,1);
}

void MagBuilder::generujZbran() {
    Zbran* staff = new Zbran ("Demonicka hul",10);
    m_hrac->seberZbran(staff);
}

void MagBuilder::generujArmor() {
    Armor* cloth = new Armor("roucho",45);
    m_hrac->seberArmor(cloth);
}

void MagBuilder::generujLektvar() {
    Lektvar* spellPot = new Lektvar(10,0);
    Lektvar* deffPot = new Lektvar(0,20);


    m_hrac->seberLektvar(spellPot);
    m_hrac->seberLektvar(deffPot);
}