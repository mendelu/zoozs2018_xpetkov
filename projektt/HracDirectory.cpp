//
// Created by Komaba on 08/12/2018.
//

#include "HracDirectory.h"

HracDirector::HracDirector(HracBuilder *builder) {
    m_builder = builder;
}

void HracDirector::setBuilder(HracBuilder *builder) {
    m_builder = builder;
}

Hrac * HracDirector::createHrdina(std::string jmeno) {
    m_builder->createHrac(jmeno);
    m_builder->generujZbran();
    m_builder->generujArmor();
    m_builder->generujLektvar();
    return m_builder->getHrac();

}
