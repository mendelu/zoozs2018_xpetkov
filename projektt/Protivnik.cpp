//
// Created by Komaba on 10/12/2018.
//

#include "Protivnik.h"

Protivnik::Protivnik(std::string jmeno, int sila, int zdravi, int lvl) : Postava(jmeno, sila, zdravi, lvl) {

}
bool Protivnik::Zije() {
    return m_zdravi > 0;
}

int Protivnik::getSilaUtoku() {
    return m_sila;
}

int Protivnik::getZraneni(int kolik) {
    return m_zdravi -= kolik;
}

int Protivnik::getZdravi() {
    return m_zdravi;
}

std::string Protivnik::getJmeno() {
    return m_jmeno;
}

void Protivnik::printInfo() {
    std::cout<<"jmeno:      "<<m_jmeno<<std::endl;
    std::cout<<"sila:       "<<m_sila<<std::endl;
    std::cout<<"zdravi:     "<<m_zdravi<<std::endl;
    std::cout<<"uroven:     "<<m_lvl<<std::endl;

}