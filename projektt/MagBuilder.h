//
// Created by Komaba on 08/12/2018.
//

#ifndef PROJEKTT_MAGBUILDER_H
#define PROJEKTT_MAGBUILDER_H

#include <HracBuilder.h>

class MagBuilder: public HracBuilder {
public:
    MagBuilder();
    void createHrac(std::string jmeno);
    void generujZbran();
    void generujArmor();
    void generujLektvar();

};


#endif //PROJEKTT_MAGBUILDER_H
