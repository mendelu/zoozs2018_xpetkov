//
// Created by Komaba on 05/12/2018.
//

#include "Hrac.h"

Hrac::Hrac(std::string jmeno, std::string povolani, int sila, int zdravi,  int obrana, int lvl) : Postava(jmeno, sila, zdravi, lvl){

    m_povolani = povolani;
    m_obrana = obrana;
    m_jidlo = 100;


}

int Hrac::getSilaUtoku() {
    int BonusSily = 0;
    for (Zbran* zbran :m_zbrane){
        BonusSily += zbran->getBonusUtok();

    }
    return  (m_sila+BonusSily) * m_lvl;
}


int Hrac::getZdravi(){
    int BonusObrany = 0;
    for(Armor* armor : m_armory){
    BonusObrany += armor->getBonusObrana();

}
    return (m_zdravi +BonusObrany + m_obrana)* m_lvl;
}



int Hrac::getLvl() {
    return m_lvl;
}



int Hrac::getJidlo() {
    return m_jidlo;
}

int Hrac::getZraneni(int kolik) {
    return m_zdravi = m_zdravi - kolik;
}

void Hrac::Boj(Protivnik *skym) {

    while ((skym->Zije()) && (Zije())) {
        skym->getZraneni(getSilaUtoku());
        std::cout<<std::endl;
        std::cout << "Zranil jsi ho za: " << getSilaUtoku() << " zivotu" << std::endl;
        std::cout << "Protivnikovi zbyva: " << skym->getZdravi() << " zivotu" << std::endl;
        if (skym->Zije()) {

            getZraneni(skym->getSilaUtoku());
            std::cout << "Zranil te za: " << skym->getSilaUtoku() << " zivotu" << std::endl;
            std::cout << "Tobe zbyva: " << getZdravi() << " zivotu" << std::endl;
        } else {
            std::cout<<std::endl;
            std::cout << "Uz jsi ho zabil     " << std::endl;
        }


    }
        if (getZdravi()>skym->getZdravi()){

            LvlUp();
            std::cout<<std::endl;
            std::cout<<"Zvitezil jsi"<<std::endl;
            std::cout<<std::endl;

        }
        else{
            std::cout<<std::endl;
            std::cout<<"Umrel jsi"<<std::endl;
            std::cout<<std::endl;
    }
}


void Hrac::seberZbran(Zbran *zbran) {
    m_zbrane.push_back(zbran);
}

void Hrac::seberLektvar(Lektvar *lektvar) {
    m_lektvary.push_back(lektvar);
}

void Hrac::seberArmor(Armor *armor) {
    m_armory.push_back(armor);
}

void Hrac::vypijLektvar() {
    //m_lektvary.pop_back();
    m_zdravi+= 10;

}
void Hrac::printInfo() {

    std::cout<<"jmeno:      "<<m_jmeno<<std::endl;
    std::cout<<"povolani:   "<<m_povolani<<std::endl;
    std::cout<<"sila:       "<<getSilaUtoku()<<std::endl;
    std::cout<<"zdravi:     "<<getZdravi()<<std::endl;
    std::cout<<"jidlo:      "<<m_jidlo<<std::endl;
    std::cout<<"uroven:     "<<m_lvl<<std::endl;

}
bool Hrac::Zije() {
    return getZdravi() > 0;
}

void Hrac::LvlUp(){
    m_lvl++;



}
