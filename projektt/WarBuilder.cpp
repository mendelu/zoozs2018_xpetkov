//
// Created by Komaba on 10/12/2018.
//

#include "WarBuilder.h"

WarBuilder::WarBuilder() {

}

void WarBuilder::createHrac(std::string jmeno) {
    m_hrac = new Hrac(jmeno, "War", 10, 20, 100, 1);
}
void WarBuilder::generujZbran() {
    Zbran* sekera = new Zbran ("Vostra sekera",30);
    m_hrac->seberZbran(sekera);
}

void WarBuilder::generujArmor() {
    Armor* plate = new Armor("plate",45);
    m_hrac->seberArmor(plate);
}

void WarBuilder::generujLektvar() {
    Lektvar *meleePow = new Lektvar(20, -10);
    Lektvar *deffPot = new Lektvar(0, 20);


    m_hrac->seberLektvar(meleePow);
    m_hrac->seberLektvar(deffPot);
}