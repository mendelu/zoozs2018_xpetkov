//
// Created by Komaba on 19/12/2018.
//

#ifndef PROJEKTT_PROTIVNIKBUILDER_H
#define PROJEKTT_PROTIVNIKBUILDER_H

#include <HracBuilder.h>
#include "Hrac.h"
#include "Postava.h"
class ProtivnikBuilder:public HracBuilder {


public:
    ProtivnikBuilder();
    void createHrac(Hrac*hrac);
    void generujZbran();
    void generujArmor();


};




#endif //PROJEKTT_PROTIVNIKBUILDER_H
